const fs = require('fs')

const error = new Error("No such file or directory is found.")

const readingLipsumFile = (callbackFunction) => {
    try{
        fs.readFile("../textFiles/lipsum.txt", (err,data) => {
            err ? callbackFunction(error.message) : callbackFunction(data.toString())
        })
    }catch{
        console.log("Something went wrong while reading a file")
    }
}

const convertingToUpperCase = (callbackFunction) => {
    try {
        fs.readFile('../textFiles/lipsum.txt',(err, data)=>{
            if (err) {
                callbackFunction(error.message)
            } else {
                const upperCaseFileContent = data.toString().toUpperCase()
                fs.writeFile('../textFiles/upperCaseContent.txt', upperCaseFileContent, (err) => {
                    if (err) {
                        callbackFunction(`While writing to a file: ${error.message}`)
                    } else {
                        callbackFunction(null,"Text is converted successfully to the upper case.")
                        fs.appendFile('../textFiles/filenames.txt', "upperCaseContent.txt " ,(err)=>{
                            if (err) {
                                callbackFunction(`While appending to a File: ${error.message}`)
                            } else {
                                callbackFunction("File name is added successfully to the filenames.txt file.")
                            }
                        })
                        
                    } 
                })
            }
        });
    } catch {
        console.log("Something went wrong while converting to upper case.")
    }
}

const convertingToLowerCaseAndSplitting = (callbackFunction) => {
    try {
        fs.readFile('../textFiles/upperCaseContent.txt',(err, data)=>{
            if (err) {
                callbackFunction(error.message)
            } else {
                const lowerCaseFileContent = data.toString().toLowerCase().split(".").join("\n")
                fs.writeFile('../textFiles/lowerCaseLipsum.txt', lowerCaseFileContent, (err) => {
                    if (err) {
                        callbackFunction(`While writing to a file: ${error.message}`)
                    } else {
                        callbackFunction(null,"Text is converted to the lower case and splitted into sentences.")
                        fs.appendFile("../textFiles/filenames.txt","lowerCaseLipsum.txt ",(err)=>{
                            if (err) {
                                callbackFunction(`While appending to a file: ${error.message}`)
                            } else {
                                callbackFunction(null,"File name is added successfully to the filenames.txt file.")
                            }
                        })
                    }
                })
            }
        });
    } catch {
        console.log("Something went wrong while converting to lower case and splitting into sentences")
    }
}

const sortingTheFileContent = (callbackFunction) => {
    try {
        fs.readFile('../textFiles/lowerCaseLipsum.txt',(err, data)=>{
            if (err) {
                callbackFunction(`While reading a file: ${error.message}`)
            } else {
                const sortedContent = data.toString().split("\n").sort().join("\n")
                fs.writeFile('../textFiles/sortedLipsum.txt', sortedContent , (err) => {
                    if (err) {
                        callbackFunction(`While writing to a file: ${error.message}`)
                    } else {
                        callbackFunction(null,"Text in the file is sorted.")
                        fs.appendFile("../textFiles/filenames.txt","sortedLipsum.txt ",(err)=>{
                            if (err) {
                                callbackFunction(`While appending to a file: ${error.message}`)
                            } else {
                                callbackFunction(null,"File name is added successfully to the filenames.txt file.")
                            }
                        })
                    }
                })
            }
        });
    } catch {
        console.log("Something went wrong while sorting the content in the file.")
    }
}

const deletingFiles = (callbackFunction) => {
    try {
        fs.readdir('../textFiles', (err, files) => {
            if (err) {
                callbackFunction(`While reading a directory: ${error.message}`)
            } else {
                fs.readFile('../textFiles/filenames.txt',(err,data)=>{
                    if (err) {
                        callbackFunction(`While reading a file: ${error.message}`)
                    } else {
                        const files = data.toString().trim().split(" ")
                        files.forEach((filename)=>{
                            if (files.includes(filename)) {
                                fs.unlink(`../textFiles/${filename}`,(err) => {
                                    if (err){
                                        callbackFunction(`While deleting a file: ${filename} ${error.message}`)
                                    } else {
                                        callbackFunction(null,`${filename} is deleted successfully.`)
                                    }
                                })
                            } else {
                                callbackFunction(`File ${filename} you want to delete is not exists`)
                            }
                        })
                    }
                })
            }
        });
    } catch {
        console.log("Something went wrong while deleting the files.")
    }
}

const problem2 = (callbackFunction) => {

    try {
        
        setTimeout(()=>{
            readingLipsumFile(callbackFunction)
        }, 2 * 1000 )

        setTimeout(()=>{
            convertingToUpperCase(callbackFunction)
        }, 2 * 1000 )

        setTimeout(()=>{
            convertingToLowerCaseAndSplitting(callbackFunction)
        }, 3 * 1000 )

        setTimeout(()=>{
            sortingTheFileContent(callbackFunction)
        }, 4 * 1000 )

        setTimeout(()=>{
            deletingFiles(callbackFunction)
        }, 5 * 1000 )

    } catch {
        console.log("Something went wrong")
    };
}



module.exports = problem2