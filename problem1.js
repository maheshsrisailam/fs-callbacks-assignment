const fs = require("fs")

const problem1 = () => {
    try {
        
        fs.mkdir('../jsonFiles', (err) => {
            err ? console.log("File Already exists") : console.log("jsonFiles directory is created.")
    
            fs.writeFile('../jsonFiles/fileOne.json', JSON.stringify([1,2,3,4,5,6,7]),(err)=>{
                err ? console.log(err) : console.log("fileOne.json file is created.")
            })
            
            fs.writeFile('../jsonFiles/fileTwo.json', JSON.stringify({name: "Mahesh", pincode:515661}),(err)=>{
                err ? console.log(err) : console.log("fileTwo.json file is created.")
            })
            
            fs.writeFile('../jsonFiles/fileThree.json', JSON.stringify([1,2,3]),(err)=>{
                err ? console.log(err) : console.log("fileThree.json file is created.")
            })
        });
    
        
        setTimeout(() => {
            fs.readdir('../jsonFiles',(err,files) => {
                if (err) {
                    console.log("No such file or directory is found")
                } else {
                    files.forEach((fileName)=> {
                        fs.unlink(`../jsonFiles/${fileName}`,(err) => {
                            if (err) {
                                console.log("Not Found: No such file exists")
                            } else {
                                console.log(`Deleted filename is ${fileName}`)
                            }
                        })
                    })
                }
            })
        },2*1000)
    } catch {
        console.log("Something went wrong")
    }
}

module.exports = problem1