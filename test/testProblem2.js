const problem2 = require("../problem2")
const callbackFunction = (error, result) => error ? console.log(error) : console.log(result)
problem2(callbackFunction)